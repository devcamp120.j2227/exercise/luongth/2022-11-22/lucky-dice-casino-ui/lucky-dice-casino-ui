// Khai báo thư viên Express
const express = require("express");
// Khai báo thư viện Mongoose
const mongoose = require("mongoose");

// Khởi tạo app express
const app = express();

// Khai báo cổng chạy app 
const port = 8000;
// Khai báo router app

const userRouter = require("./app/routes/userRouter");

const historyDiceRouter = require("./app/routes/historydiceRouter");

const prizeRouter = require("./app/routes/prizeRouter");

const voucherRouter = require("./app/routes/voucherRouter");

const viewRouter = require("./app/routes/viewRouter");

const randomRouter = require("./random");

const prizeHistoryRouter = require("./app/routes/prizeHistoryRouter");

const voucherHistoryRouter = require("./app/routes/voucherHistoryRouter");

const diceRouter = require("./app/routes/diceRouter");




// Cấu hình request đọc được body json
app.use(express.json());

app.use((request, response, next) => {
    console.log("Current time: ", new Date());
    next();
})

app.use((request, response, next) => {
    console.log("Request method: ", request.method);
    next();
})

mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Course", (error) => {
    if(error) throw error;
    console.log("Connect MongoDB successfully!");
})

// Khai báo API /
app.get("/", (request, response) => {
    console.log("Call API GET /");

    response.json({
        message: "Devcamp Great Express APP"
    })
})
// App sử dụng router
app.use("/api", userRouter);

app.use("/api", historyDiceRouter);

app.use("/api", prizeRouter);

app.use("/api", voucherRouter);

app.use("/api", viewRouter);

app.use("/api", randomRouter);

app.use("/api", prizeHistoryRouter);

app.use("/api", voucherHistoryRouter);

app.use("/api", diceRouter);

// Chạy app trên cổng
app.listen(port, () => {
    console.log("App listening on port:", port);
}) 