const express = require('express');
const router = express.Router();

// Import middleware
// const signInMiddleware = require('../middleware/signInMiddleware')
// Get a random entry
var getRandomIntInclusive = (min, max) =>{
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1) + min); // The maximum is inclusive and the minimum is inclusive
  }
router.get("/randoms",  (req, res) => {
    res.status(200).json({
        status: "Get all random successfully",
        data: getRandomIntInclusive(1,6)
    })
})



module.exports = router;
