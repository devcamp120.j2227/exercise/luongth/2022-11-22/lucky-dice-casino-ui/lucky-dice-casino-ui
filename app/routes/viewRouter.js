const express = require('express');
const router = express.Router();
const path = require('path');
// Import middleware

// const signInMiddleware = require('../middleware/signInMiddleware')

router.get("/",  (req, res) => {
    res.sendFile(path.join(process.cwd() + "/app/views/luckyDiceCasino.html" ))
})

router.use(express.static(process.cwd() + '/app/views'))

module.exports = router;
