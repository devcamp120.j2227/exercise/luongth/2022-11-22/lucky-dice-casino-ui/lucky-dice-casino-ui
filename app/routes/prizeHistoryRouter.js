// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import PrizeHistory middleware
const PrizeMiddleware = require("../middlewares/prizeHistoryMiddlewares");
// Import PrizeHistory controlles
const prizeHistoryControlles = require("../controllers/prizeHistory.controlles");


router.get("/prize-histories",PrizeMiddleware.getAllPrizeHistoryMiddleware,prizeHistoryControlles.getAllHistoryPrize);

router.get("/limit-prize",PrizeMiddleware.getFindlPrizeHistoryMiddleware,prizeHistoryControlles.getAllfindHistoryPrize);

router.post("/prize-histories",PrizeMiddleware.createPrizeHistoryMiddleware,prizeHistoryControlles.createHistoryPrize);

router.get("/prize-histories/:historiesId",PrizeMiddleware.getDetailPrizeHistoryMiddleware,prizeHistoryControlles.getHistoryPrizeById)

// router.put("/prize-histories/:historiesId",PrizeMiddleware.updatePrizeHistoryMiddleware,prizeHistoryControlles.updateHistoryPrize)

router.delete("/prize-histories/:historiesId",PrizeMiddleware.deletePrizeHistoryMiddleware,prizeHistoryControlles.deleteHistoryPrize)
module.exports = router;