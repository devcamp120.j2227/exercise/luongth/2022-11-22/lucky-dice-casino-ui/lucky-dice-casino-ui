// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import DiceHistory middleware
const VoucherMiddleware = require("../middlewares/voucherMiddlewares");
// Import DiceHistory controlles
const voucherControlles = require("../controllers/voucher.controlles");


router.get("/vouchers",VoucherMiddleware.getAllVoucherMiddleware,voucherControlles.getAllVoucher);

router.post("/voucher-histories/:historyId/vouchers/",VoucherMiddleware.createVoucherMiddleware,voucherControlles.createVoucher);

router.get("/vouchers/:voucherId",VoucherMiddleware.getDetailVoucherMiddleware,voucherControlles.getVoucherById)

router.put("/vouchers/:voucherId",VoucherMiddleware.updateVoucherMiddleware,voucherControlles.updateVoucher)

router.delete("/voucher-histories/:historyId/vouchers/:voucherId",VoucherMiddleware.deleteVoucherMiddleware,voucherControlles.deleteVoucherId)
module.exports = router;