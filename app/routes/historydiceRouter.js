// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import DiceHistory middleware
const DiceHistoryMiddleware = require("../middlewares/historyDiceMiddleware");
// Import DiceHistory controlles
const DiceHistoryControlles = require("../controllers/historydice.controlles");


router.get("/historyDices",DiceHistoryMiddleware.getAllDiceHistoryMiddleware,DiceHistoryControlles.getAllHistoryDice);

router.get("/limit",DiceHistoryMiddleware.getfindDiceHistoryMiddleware,DiceHistoryControlles.getAllfindHistoryDice);

router.post("/historyDices",DiceHistoryMiddleware.createDiceHistoryMiddleware,DiceHistoryControlles.createHistoryDice);

router.get("/historyDices/:diceId",DiceHistoryMiddleware.getDetailDiceHistoryMiddleware,DiceHistoryControlles.getHistoryDiceById)

router.put("/historyDices/:diceId",DiceHistoryMiddleware.updateDiceHistoryMiddleware,DiceHistoryControlles.updateHistoryDiceId)

router.delete("/historyDices/:diceId",DiceHistoryMiddleware.deleteDiceHistoryMiddleware,DiceHistoryControlles.deleteHistoryDiceID)
module.exports = router;