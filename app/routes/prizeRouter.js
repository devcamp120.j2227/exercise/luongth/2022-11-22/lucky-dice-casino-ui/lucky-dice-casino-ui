// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import DiceHistory middleware
const PrizeMiddleware = require("../middlewares/historyDiceMiddleware");
// Import Prize controlles
const PrizeControlles = require("../controllers/prize.controlles");


router.get("/prizes",PrizeMiddleware.getAllDiceHistoryMiddleware,PrizeControlles.getAllPrize);

router.post("/prize-histories/:historiesId/prizes",PrizeMiddleware.createDiceHistoryMiddleware,PrizeControlles.createPrize);

router.get("/prizes/:prizeId",PrizeMiddleware.getDetailDiceHistoryofUserMiddleware,PrizeControlles.getPrizeById)

router.put("/prizes/:prizeId",PrizeMiddleware.updateDiceHistoryMiddleware,PrizeControlles.updatePrice)

router.delete("/prize-histories/:historiesId/prizes/:prizeId",PrizeMiddleware.deleteDiceHistoryMiddleware,PrizeControlles.deletePrizeId)
module.exports = router;