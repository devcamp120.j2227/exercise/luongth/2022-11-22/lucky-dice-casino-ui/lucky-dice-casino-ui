// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import user middleware
const userMiddleware = require("../middlewares/userMiddleware");

// Import user controller
const userController = require("../controllers/user.controlles")

router.get("/users",userMiddleware.getAllUserMiddleware,userController.getAllUser);

router.post("/historyDices/:diceId/prize-histories/:historiesId/voucher-histories/:historyId/users",userMiddleware.createUserMiddleware,userController.createUser);

router.get("/historyDices/:diceId/users",userMiddleware.createUserMiddleware,userController.getAllUserOfHistoryDice);

router.get("/users/:userId", userMiddleware.getDetailUserMiddleware,userController.getUserById)

router.put("/users/:userId",userMiddleware.updateUserMiddleware,userController.updateUserById)

router.delete("/historyDices/:diceId/prize-histories/:historiesId/voucher-histories/:historyId/users/:userId",userMiddleware.deleteUserMiddleware,userController.deleteUserByID)

module.exports = router;