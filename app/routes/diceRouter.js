// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import DiceHistory middleware
const diceMiddleware = require("../middlewares/dicemiddlewarers");
// Import Prize controlles
const diceControlles = require("../controllers/dice .controlles");


router.get("/devcamp-lucky-dice/dice",diceMiddleware.getAllDiceMiddleware,diceControlles.getDicHistoryUSer);

router.post("/devcamp-lucky-dice/dice",diceMiddleware.createDiceMiddleware,diceControlles.diceHandler);

router.get("/devcamp-lucky-dice/voucher-history",diceMiddleware.getDetailDiceMiddleware,diceControlles.getVoucherHistoryUSer)

 router.get("/devcamp-lucky-dice/prize-history",diceMiddleware.updateDiceMiddleware,diceControlles.getPizeHistoryUSer)

// router.delete("/prize-histories/:historiesId/prizes/:prizeId",diceMiddleware.deleteDiceMiddleware,PrizeControlles.deletePrizeId)
module.exports = router;