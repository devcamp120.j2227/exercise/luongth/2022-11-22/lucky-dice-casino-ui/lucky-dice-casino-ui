// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import VoucherHistory middleware
const VoucherHistoryMiddleware = require("../middlewares/voucherHistoryMiddleware");
// Import VoucherHistory controlles
const voucherHistoryControlles = require("../controllers/voucherHistory.Controlles");


router.get("/voucher-histories",VoucherHistoryMiddleware.getAllVoucherHistoryMiddleware,voucherHistoryControlles.getAllHistoryVoucher);

router.get("/limit-voucher",VoucherHistoryMiddleware.getFindVoucherHistoryMiddleware,voucherHistoryControlles.getAllHistoryVoucher);

router.post("/voucher-histories",VoucherHistoryMiddleware.createVoucherHistoryMiddleware,voucherHistoryControlles.createHistoryVoucher);

router.get("/voucher-histories/:historyId",VoucherHistoryMiddleware.getDetailVoucherHistoryMiddleware,voucherHistoryControlles.getHistoryVoucherById)

//router.put("/voucher-histories/:historyId",VoucherHistoryMiddleware.updateVoucherHistoryMiddleware,voucherHistoryControlles.updateHistoryVoucher)

router.delete("/voucher-histories/:historyId",VoucherHistoryMiddleware.deleteVoucherHistoryMiddleware,voucherHistoryControlles.deleteHistoryVoucher)
module.exports = router;