// Khai báo thư viên mongooseJS
const mongoose = require("mongoose");

// Khai báo Class Schema
const Schema = mongoose.Schema;

// Khởi tạo 1 instance courseSchema từ class Schema
const userSchema = new Schema({
    username: {
        type: String,
        required: true,
        unique:true
    },
    firstname: {
        type: String,
        required:true
    },
    lastname: {
        type: String,
        required:true
    },
    createdAt: {
        type: Date,
        default:Date.now()
    },
    updatedAt: {
        type: Date,
        default:Date.now()
    }
}, {
    timestamps: true
})

module.exports = mongoose.model("User", userSchema);