//Import thư viện mongoose
const mongoose = require("mongoose");

//Class Schema từ thư viện mongoose
const Schema = mongoose.Schema;

//Khởi tạo instance prizeSchema từ Class Schema
const diceSchema = new Schema({
     
    voucher: [{
        type: mongoose.Types.ObjectId,
        ref: "Voucher",
        required:true
    }],
    dice: {
        type: Number,
        required: true
    },
    prize:{
        type: String,
        required:true
    },
    createdAt: {
        type: Date,
        default:Date.now()
    },
    updatedAt: {
        type: Date,
        default:Date.now()
    }
}, {
    // Ghi dấu bản ghi được tạo hay cập nhật vào thời gian nào
    timestamps: true
});
// Biên dịch Review Model từ reviewSchema
module.exports = mongoose.model("Dice", diceSchema);