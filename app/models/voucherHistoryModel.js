//Import thư viện mongoose
const mongoose = require("mongoose");

//Class Schema từ thư viện mongoose
const Schema = mongoose.Schema;

//Khởi tạo instance reviewSchema từ Class Schema
const voucherHistorySchema = new Schema({
      // Một user có nhiều car
      user: {
        type: mongoose.Types.ObjectId,
        ref: "User",
        required:true
    },
    voucher: {
        type: mongoose.Types.ObjectId,
        ref: "Voucher",
        required:true
    },
    createdAt: {
        type: Date,
        default:Date.now()
    },
    updatedAt: {
        type: Date,
        default:Date.now()
    }
}, {
    // Ghi dấu bản ghi được tạo hay cập nhật vào thời gian nào
    timestamps: true
});

// Biên dịch Review Model từ reviewSchema
module.exports = mongoose.model("HistoryVoucher", voucherHistorySchema);