const getAllDiceMiddleware = (request, response, next) => {
    console.log("Get ALL Dice Middleware");
    next();
}

const createDiceMiddleware = (request, response, next) => {
    console.log("Create Dice Middleware");
    next();
}

const getfindDiceMiddleware = (request, response, next) => {
    console.log("Get Find Dice Middleware");
    next();
}

const getDetailDiceMiddleware = (request, response, next) => {
    console.log("Get Detail Dice Middleware");
    next();
}
const getDetailDiceofUserMiddleware = (request, response, next) => {
    console.log("Get Detail Dice of user Middleware");
    next();
}

const updateDiceMiddleware = (request, response, next) => {
    console.log("Update Dice Middleware");
    next();
}

const deleteDiceMiddleware = (request, response, next) => {
    console.log("Delete Dice Middleware");
    next();
}

module.exports = {
    getAllDiceMiddleware,
    createDiceMiddleware,
    getDetailDiceMiddleware,
    getfindDiceMiddleware,
    getDetailDiceofUserMiddleware,
    updateDiceMiddleware,
    deleteDiceMiddleware
}
