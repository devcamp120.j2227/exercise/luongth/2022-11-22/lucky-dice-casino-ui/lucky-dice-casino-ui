const getAllPrizeHistoryMiddleware = (request, response, next) => {
    console.log("Get ALL PrizeHistory Middleware");
    next();
}

const createPrizeHistoryMiddleware = (request, response, next) => {
    console.log("Create PrizeHistory Middleware");
    next();
}

const getDetailPrizeHistoryMiddleware = (request, response, next) => {
    console.log("Get Detail PrizeHistory Middleware");
    next();
}
const getDetailPrizeHistoryofUserMiddleware = (request, response, next) => {
    console.log("Get Detail PrizeHistory of user Middleware");
    next();
}

const getFindlPrizeHistoryMiddleware = (request, response, next) => {
    console.log("Get Find PrizeHistory Middleware");
    next();
}


const updatePrizeHistoryMiddleware = (request, response, next) => {
    console.log("Update PrizeHistory Middleware");
    next();
}

const deletePrizeHistoryMiddleware = (request, response, next) => {
    console.log("Delete PrizeHistory Middleware");
    next();
}

module.exports = {
    getAllPrizeHistoryMiddleware,
    createPrizeHistoryMiddleware,
    getDetailPrizeHistoryMiddleware,
    getFindlPrizeHistoryMiddleware,
    getDetailPrizeHistoryofUserMiddleware,
    updatePrizeHistoryMiddleware,
    deletePrizeHistoryMiddleware
}
