const getAllDiceHistoryMiddleware = (request, response, next) => {
    console.log("Get ALL DiceHistory Middleware");
    next();
}

const createDiceHistoryMiddleware = (request, response, next) => {
    console.log("Create DiceHistory Middleware");
    next();
}

const getfindDiceHistoryMiddleware = (request, response, next) => {
    console.log("Get Find DiceHistory Middleware");
    next();
}

const getDetailDiceHistoryMiddleware = (request, response, next) => {
    console.log("Get Detail DiceHistory Middleware");
    next();
}
const getDetailDiceHistoryofUserMiddleware = (request, response, next) => {
    console.log("Get Detail DiceHistory of user Middleware");
    next();
}

const updateDiceHistoryMiddleware = (request, response, next) => {
    console.log("Update DiceHistory Middleware");
    next();
}

const deleteDiceHistoryMiddleware = (request, response, next) => {
    console.log("Delete DiceHistory Middleware");
    next();
}

module.exports = {
    getAllDiceHistoryMiddleware,
    createDiceHistoryMiddleware,
    getDetailDiceHistoryMiddleware,
    getfindDiceHistoryMiddleware,
    getDetailDiceHistoryofUserMiddleware,
    updateDiceHistoryMiddleware,
    deleteDiceHistoryMiddleware
}
