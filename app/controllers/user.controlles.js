// Import thư viện Mongoose
const mongoose = require("mongoose");


// Import Module HistoryDice Model
const historyDiceModel = require("../models/dicehistoryModel");

// Import Module HistoryPrize Model
const historyPrizeModel = require("../models/prizeHHistoryModel");

// Import Module HistoryVoucher Model
const historyVoucherModel = require("../models/voucherHistoryModel")

// Import Module Course Model
const userModel = require("../models/userModel");

const getAllUser = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    userModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get all user successfully",
            data: data
        })
    })
}

const createUser = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const body = request.body;
    const diceId = request.params.diceId;
      
    const HistoryPrizeId = request.params.historiesId;

    const HistoryId = request.params.historyId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(diceId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "History Dice ID không hợp lệ"
        })
    }

    if (!mongoose.Types.ObjectId.isValid(HistoryPrizeId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "History Dice ID không hợp lệ"
        })
    }
    if (!mongoose.Types.ObjectId.isValid(HistoryId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "History Dice ID không hợp lệ"
        })
    }
    // Kiểm tra username có hợp lệ hay không
    if(!body.username) {
        return response.status(400).json({
            status: "Bad Request",
            message: "username không hợp lệ"
        })
    }
    // Kiểm tra firstname có hợp lệ hay không
    if(!body.firstname) {
        return response.status(400).json({
            status: "Bad Request",
            message: "firstname không hợp lệ"
        })
    }
    // Kiểm tra lastname có hợp lệ hay không
    if(!body.lastname) {
        return response.status(400).json({
            status: "Bad Request",
            message: "lastname không hợp lệ"
        })
    }
    // B3: Gọi Model tạo dữ liệu
    const newUser = {
        _id: mongoose.Types.ObjectId(),
        username: body.username,
        firstname: body.firstname,
        lastname: body.lastname,
    }
    console.log(newUser)
   userModel.create(newUser, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        historyDiceModel.findByIdAndUpdate(diceId, {
            $push: {
                user: data._id
            }
        },
         (err, updatedUser) => {
            if (err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }
            return response.status(201).json({
                status: "Create history Users Successfully",
                data: data
            })
        })
        historyPrizeModel.findByIdAndUpdate(HistoryPrizeId, {
            $push: {
                user: data._id
            }
        },
         (err, updatedUser) => {
            if (err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }
           
        }),
        historyVoucherModel.findByIdAndUpdate(HistoryId, {
            $push: {
                user: data._id
            }
        },
         (err, updatedUser) => {
            if (err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }
           
        })
    })
}

const getAllUserOfHistoryDice = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const DiceId = request.params.diceId;
    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(DiceId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "User ID không hợp lệ"
        })
    }

    // B3: Thao tác với cơ sở dữ liệu
     historyDiceModel.findById(DiceId)
        .populate("user")
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }
            return response.status(200).json({
                status: "Get all user of historyDiec successfully",
                data: data
            })
        })
}

const getUserById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const UserId = request.params.userId;
    
    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(UserId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "UserId không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    userModel.findById(UserId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get by id  User successfully",
            data: data
        })
    })
}

const updateUserById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const UserId = request.params.userId;
    const body = request.body;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(UserId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "UserId không hợp lệ"
        })
    }

    if(body.username!== undefined && body.username.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "username không hợp lệ"
        })
    }

    if(body.firstname!== undefined && body.firstname.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "firstname không hợp lệ"
        })
    }


    if(body.lastname!== undefined && body.lastname.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "lastname không hợp lệ"
        })
    }
   

    // B3: Gọi Model tạo dữ liệu
    const updateUser = {}

    if(body.username !== undefined) {
        updateUser.username = body.username
    }

    if(body.firstname !== undefined) {
        updateUser.firstname = body.firstname
    }

    if(body.lastname !== undefined) {
        updateUser.lastname = body.lastname
    }

    userModel.findByIdAndUpdate(UserId, updateUser, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Update User successfully",
            data: data
        })
    })
}

const deleteUserByID = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const UserId = request.params.userId;

    const DiceId = request.params.diceId;

    const HistoryPrizeId = request.params.historiesId;

    const HistoryId = request.params.historyId;


    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(UserId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "UserId không hợp lệ"
        })
    }
 // B2: Validate dữ liệu
 if(!mongoose.Types.ObjectId.isValid(DiceId)) {
    return response.status(400).json({
        status: "Bad Request",
        message: "UserId không hợp lệ"
    })
}
    // B3: Gọi Model tạo dữ liệu
    userModel.findByIdAndDelete(UserId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

         // Sau khi xóa xong user khỏi collection cần cóa thêm 
     historyDiceModel.findByIdAndUpdate(DiceId, {
            $pull: { user: UserId }
        }, (err, updatedCar) => {
            if (err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }

            return response.status(204).json({
                status: "Delete  by Id user successfully"
            })
        })
          historyPrizeModel.findByIdAndUpdate(HistoryPrizeId, {
            $pull: { user: UserId }
        }, (err, updatedCar) => {
            if (err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }
        })
        historyVoucherModel.findByIdAndUpdate(HistoryId, {
            $pull: { user: UserId }
        }, (err, updatedCar) => {
            if (err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }
        })
    })
}

module.exports = {
    getAllUser,
    createUser,
    getUserById,
    getAllUserOfHistoryDice,
    updateUserById,
    deleteUserByID
}