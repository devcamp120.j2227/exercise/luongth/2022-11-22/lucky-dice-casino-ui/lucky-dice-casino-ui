
// Import thư viện Mongoose
const mongoose = require("mongoose");

const prizeModel = require("../models/prizeModel");
// Import Module HistoryPrize Model
const historyPrizeModel = require("../models/prizeHHistoryModel");

// Import Module HistoryDice Model
const getAllPrize = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    prizeModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all  Prize successfully",
            data: data
        })
    })
}

const createPrize = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const PrizeHistoryId = request.params.historiesId;
    const body = request.body;

    console.log(body)
    // B2: Validate dữ liệu

    // B3: Gọi dice tạo dữ liệu
    const newPrize = {
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        description: body.description
       
    }
   prizeModel.create(newPrize, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        historyPrizeModel.findByIdAndUpdate(PrizeHistoryId, {
            $push: {
                prize: data._id
            }
        },
         (err, updatedPrize) => {
            if (err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }
            return response.status(200).json({
                status: "Get  by Id Prize successfully",
                data: data
            })
        })
    })
};

const getPrizeById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const PriceId = request.params.prizeId;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(PriceId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "PriceId không hợp lệ"
        })
    }
    
    // B3: Gọi Model tạo dữ liệu
prizeModel.findById(PriceId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        
       
    })
}
const updatePrice = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const PriceId = request.params.prizeId;

    const body = request.body;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(PriceId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "PriceId không hợp lệ"
        })
    }

    if(body.name !== undefined && body.name === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "name không hợp lệ"
        })
    }
    

    // B3: Gọi Model tạo dữ liệu
    const updatePrice = {}

    if(body.name !== undefined) {
        updatePrice.name = body.name
    }

   prizeModel.findByIdAndUpdate(PrizeId, updatePrice, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Update by id Prize successfully",
            data: data
        })
    })
};

const deletePrizeId = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const PriceId = request.params.prizeId;
    
    const PrizeHistoryId = request.params.historiesId;
    
    // B2: Validate dữ liệu
      //vlidate DiceId
    if(!mongoose.Types.ObjectId.isValid(PriceId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "PriceId không hợp lệ"
        })
    }
    
    // B3: Gọi Model tạo dữ liệu
    prizeModel.findByIdAndDelete(PriceId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        historyPrizeModel.findByIdAndUpdate(PrizeHistoryId, {
            $pull: { prize:PriceId }
        }, (err, updatedCar) => {
            if (err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }
            return response.status(204).json({
                status: "Delete  by Id Prize successfully"
            })
        })
        
       
    })
}

module.exports = {
    getAllPrize,
    createPrize,
    getPrizeById,
    updatePrice,
    deletePrizeId
}