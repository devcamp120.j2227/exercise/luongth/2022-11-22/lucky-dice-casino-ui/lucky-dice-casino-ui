
// Import thư viện Mongoose
const mongoose = require("mongoose");

// Import Module HistoryDice Model
const historyDiceModel = require("../models/dicehistoryModel");
//hàm random
var getRandomIntInclusive = (min, max) =>{
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1) + min); // The maximum is inclusive and the minimum is inclusive
  }



const createHistoryDice = (request, response) => {
    // B1: Chuẩn bị dữ liệu
   
    const body = request.body;
    console.log(body)
    // B2: Validate dữ liệu

    // B3: Gọi dice tạo dữ liệu
    const newHistoryDice = {
        _id: mongoose.Types.ObjectId(),
        dice: getRandomIntInclusive(1,6)
        
       
    }

    historyDiceModel.create(newHistoryDice, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get  by Id HistoryDice successfully",
            data: data
        })
    })
};
const getAllHistoryDice = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    historyDiceModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all  History Dice successfully",
            data: data
        })
    })
}

const getAllfindHistoryDice = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    //thu thập dữ liệu trên front-end
    let limit = request.query.limit;
    
    historyDiceModel.find()
    .limit(limit)  
    .exec((error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get courses success",
                data: data
            })
        }
    });
}
const getHistoryDiceById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const DiceId = request.params.diceId;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(DiceId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "DiceId không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
 historyDiceModel.findById(DiceId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get  by Id HistoryDice successfully",
            data: data
        })
    })
}

const updateHistoryDiceId = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const DiceId = request.params.diceId;

    const body = request.body;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(DiceId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "CarId không hợp lệ"
        })
    }

    if(body.dice !== undefined && body.dice === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "Dice không hợp lệ"
        })
    }
    

    // B3: Gọi Model tạo dữ liệu
    const updateHistoryDice = {}

    if(body.dice !== undefined) {
        updateHistoryDice.dice = body.dice
    }

   
   historyDiceModel.findByIdAndUpdate(DiceId, updateHistoryDice, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Update by id historyDice successfully",
            data: data
        })
    })
}

const deleteHistoryDiceID = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const DiceId = request.params.diceId;
    
    // B2: Validate dữ liệu
      //vlidate DiceId
    if(!mongoose.Types.ObjectId.isValid(DiceId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "DiceId không hợp lệ"
        })
    }
    
    // B3: Gọi Model tạo dữ liệu
    historyDiceModel.findByIdAndDelete(DiceId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(204).json({
            status: "Delete  by Id historyDice successfully"
        })
       
    })
}

module.exports = {
    getAllHistoryDice,
    createHistoryDice,
    getAllfindHistoryDice,
    getHistoryDiceById,
    updateHistoryDiceId,
    deleteHistoryDiceID
}