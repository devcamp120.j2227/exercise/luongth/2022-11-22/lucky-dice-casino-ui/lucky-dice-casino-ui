
// Import thư viện Mongoose
const mongoose = require("mongoose");


// Import Module HistoryDice Model
const prizeHistoryModel = require("../models/prizeHHistoryModel");

const getAllHistoryPrize = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    prizeHistoryModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all  History Prize successfully",
            data: data
        })
    })
}

const createHistoryPrize = (request, response) => {
    // B1: Chuẩn bị dữ liệu
   
    // B2: Validate dữ liệu

    // B3: Gọi dice tạo dữ liệu
    const newHistoryPrize = {
        _id: mongoose.Types.ObjectId(),
    }

    prizeHistoryModel.create(newHistoryPrize, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get  by Id HistoryPrice successfully",
            data: data
        })
    })
};

const getAllfindHistoryPrize = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    //thu thập dữ liệu trên front-end
    let limit = request.query.limit;
    
    prizeHistoryModel.find()
    .limit(limit)  
    .exec((error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get courses success",
                data: data
            })
        }
    });
}

const getHistoryPrizeById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const PrizeId = request.params.historiesId;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(PrizeId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "PrizeId không hợp lệ"
        })
    }
    
    // B3: Gọi Model tạo dữ liệu
    prizeHistoryModel.findById(PrizeId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get  by Id History Prize successfully",
            data: data
        })
    })
}

const updateHistoryPrize = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const PrizeId = request.params.historiesId;


    const body = request.body;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(PrizeId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "CarId không hợp lệ"
        })
    }    
    // B3: Gọi Model tạo dữ liệu
    const updateHistoryDice = {}

    if(body.prize !== undefined) {
        updateHistoryDice.prize = body.prize
    }
    prizeHistoryModel.findByIdAndUpdate(PrizeId, updateHistoryDice, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Update by id history Prize successfully",
            data: data
        })
    })
}

const deleteHistoryPrize = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const PrizeId = request.params.historiesId;
    // B2: Validate dữ liệu
      //vlidate DiceId
    if(!mongoose.Types.ObjectId.isValid(PrizeId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "DiceId không hợp lệ"
        })
    }
    
    // B3: Gọi Model tạo dữ liệu
    historyDiceModel.findByIdAndDelete(PrizeId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(204).json({
            status: "Delete  by Id HistoryPrize successfully"
        })
       
    })
}

module.exports = {
    getAllHistoryPrize,
    createHistoryPrize,
    getHistoryPrizeById,
    getAllfindHistoryPrize,
    updateHistoryPrize,
    deleteHistoryPrize
}