
// Import thư viện Mongoose
const mongoose = require("mongoose");


// Import Module HistoryDice Model
const voucherHistoryModel = require("../models/voucherHistoryModel");

const getAllHistoryVoucher = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    voucherHistoryModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all  History Voucher successfully",
            data: data
        })
    })
};

const getAllfindHistoryVoucher = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    //thu thập dữ liệu trên front-end
    let limit = request.query.limit;
    
    voucherHistoryModel.find()
    .limit(limit)  
    .exec((error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get courses success",
                data: data
            })
        }
    });
}

const createHistoryVoucher = (request, response) => {
    // B1: Chuẩn bị dữ liệu
   
    // B2: Validate dữ liệu

    // B3: Gọi dice tạo dữ liệu
    const voucherHistory = {
        _id: mongoose.Types.ObjectId(),
    }

    voucherHistoryModel.create(voucherHistory, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get  by Id History Voucher successfully",
            data: data
        })
    })
};

const getHistoryVoucherById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const HistoryId = request.params.historyId;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(HistoryId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "HistoryId không hợp lệ"
        })
    }
    
    // B3: Gọi Model tạo dữ liệu
    voucherHistoryModel.findById(HistoryId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get  by Id History Voucher successfully",
            data: data
        })
    })
}

const updateHistoryVoucher = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const HistoryId = request.params.historyId;

    const body = request.body;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(HistoryId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "CarId không hợp lệ"
        })
    }
    // B3: Gọi Model tạo dữ liệu
    const updateHistoryVoucher = {}

    if(body.dice !== undefined) {
        updateHistoryDice.dice = body.dice
    }

   
   voucherHistoryModel.findByIdAndUpdate(DiceId, updateHistoryVoucher, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Update by id history Voucher successfully",
            data: data
        })
    })
}

const deleteHistoryVoucher = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const HistoryId = request.params.historyId;

    // B2: Validate dữ liệu
      //vlidate DiceId
    if(!mongoose.Types.ObjectId.isValid(HistoryId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "HistoryId không hợp lệ"
        })
    }
    
    // B3: Gọi Model tạo dữ liệu
    voucherHistoryModel.findByIdAndDelete(HistoryId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(204).json({
            status: "Delete  by Id History Voucher successfully"
        })
       
    })
}

module.exports = {
    getAllHistoryVoucher,
    createHistoryVoucher,
    getHistoryVoucherById,
    getAllfindHistoryVoucher,
    updateHistoryVoucher,
    deleteHistoryVoucher
}