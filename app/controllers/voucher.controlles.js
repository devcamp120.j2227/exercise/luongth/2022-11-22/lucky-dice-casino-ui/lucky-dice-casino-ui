
// Import thư viện Mongoose
const mongoose = require("mongoose");

const voucherModel = require("../models/voucherModel");
// Import Module HistoryPrize Model
const historyVoucherModel = require("../models/voucherHistoryModel");
// Import Module HistoryDice Model
const getAllVoucher = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    voucherModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all  Prize successfully",
            data: data
        })
    })
}

const createVoucher = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const HistoryId = request.params.historyId;

    const body = request.body;
    console.log(body)
    // B2: Validate dữ liệu
 // Kiểm tra code có hợp lệ hay không
 if(!body.code) {
    return response.status(400).json({
        status: "Bad Request",
        message: "code không hợp lệ"
    })
 }
// Kiểm tra discount có hợp lệ hay không
if(isNaN(body.discount) || body.discount < 0) {
    return response.status(400).json({
        status: "Bad Request",
        message: "No discount không hợp lệ"
    })
}
    // B3: Gọi dice tạo dữ liệu
    const newCode = {
        _id: mongoose.Types.ObjectId(),
        code: body.code,
        discount: body.discount,
        note: body.note
       
    }
    voucherModel.create(newCode, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        historyVoucherModel.findByIdAndUpdate(HistoryId, {
            $push: {
                voucher: data._id
            }
        },
         (err, updatedUser) => {
            if (err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }
            return response.status(201).json({
                status: "Create history prize Successfully",
                data: data
            })
        })
    })
};

const getVoucherById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const VoucherId = request.params.voucherId;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(VoucherId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "VoucherId không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
voucherModel.findById(VoucherId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get  by Id Voucher successfully",
            data: data
        })
    })
}

const updateVoucher = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const VoucherId = request.params.voucherId;

    const body = request.body;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(VoucherId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "VoucherId không hợp lệ"
        })
    }

    if(body.code !== undefined && body.code === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "code không hợp lệ"
        })
    }
    

    // B3: Gọi Model tạo dữ liệu
    const updateVoucher = {}

    if(body.code !== undefined) {
        updateVoucher.code = body.code
    }
    if(body.discount !== undefined) {
        updateVoucher.discount = body.discount
    }
    if(body.note !== undefined) {
        updateVoucher.note = body.note
    }
   voucherModel.findByIdAndUpdate(VoucherId, updateVoucher, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Update by id Voucher successfully",
            data: data
        })
    })
};

const deleteVoucherId = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const VoucherId = request.params.voucherId;

    const HistoryId = request.params.historyId;

    // B2: Validate dữ liệu
      //vlidate DiceId
    if(!mongoose.Types.ObjectId.isValid(VoucherId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "VoucherId không hợp lệ"
        })
    }
    if(!mongoose.Types.ObjectId.isValid(HistoryId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "VoucherId không hợp lệ"
        })
    }
    // B3: Gọi Model tạo dữ liệu
    voucherModel.findByIdAndDelete(VoucherId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        historyVoucherModel.findByIdAndUpdate(HistoryId, {
            $pull: { voucher: VoucherId }
        }, (err, updatedCar) => {
            if (err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }
        })
        return response.status(204).json({
            status: "Delete  by Id VoucherId successfully"
        })
       
    })
}

module.exports = {
    getAllVoucher,
    createVoucher,
    getVoucherById,
    updateVoucher,
    deleteVoucherId
}